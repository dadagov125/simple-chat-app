import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/blocs/authentication/authentication_bloc.dart';
import 'package:chat_mobile/blocs/notify/notify_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:mockito/mockito.dart';

class FlutterSecureStorageMock extends Mock implements FlutterSecureStorage {}

class UsersClientMock extends Mock implements UsersClient {}

class AuthenticationBlocMock extends Mock implements AuthenticationBloc {}

class NotifyBlocMock extends Mock implements NotifyBloc {}

class ChatsClientMock extends Mock implements ChatsClient {}

class MessagesClientMock extends Mock implements MessagesClient {}
