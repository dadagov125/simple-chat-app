import 'package:bloc_test/bloc_test.dart';
import 'package:chat_mobile/blocs/authentication/authentication_bloc.dart';
import 'package:chat_mobile/blocs/chat/chat_bloc.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../mocks.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('chat_bloc_test', () {
    AuthenticationBlocMock authBloc;
    ChatBloc chatBloc;
    ChatsClientMock chatsClientMock;
    NotifyBlocMock notifyBlocMock;
    MessagesClientMock messagesClientMock;

    setUp(() {
      authBloc = AuthenticationBlocMock();
      chatsClientMock = ChatsClientMock();
      messagesClientMock = MessagesClientMock();
      notifyBlocMock = NotifyBlocMock();
      chatBloc = ChatBloc(
          authBloc, chatsClientMock, messagesClientMock, notifyBlocMock);
    });

    blocTest('initial state should be empty props',
        build: () => chatBloc,
        act: (ChatBloc chatBloc) {},
        expect: [],
        verify: (ChatBloc chatBloc) {
          expect(chatBloc.state.chats.length, 0);
          expect(chatBloc.state.messages.length, 0);
          expect(chatBloc.state.unreadedMessages.length, 0);
        });

    blocTest('chats is not be empty on LoadChatsRequested',
        build: () => chatBloc,
        act: (ChatBloc chatBloc) {
          when(chatsClientMock.read(any))
              .thenAnswer((realInvocation) async => [Chat(id: ChatId('id'))]);
          chatBloc.add(LoadChatsRequested());
        },
        expect: [
          ChatState([Chat(id: ChatId('id'))], {}, {})
        ],
        verify: (ChatBloc chatBloc) {
          expect(chatBloc.state.chats.length, 1);
          expect(chatBloc.state.messages.length, 0);
          expect(chatBloc.state.unreadedMessages.length, 0);
        });

    blocTest('messages is not be empty on LoadMessagesRequested',
        build: () => chatBloc,
        act: (ChatBloc chatBloc) {
          when(messagesClientMock.read(any)).thenAnswer(
              (realInvocation) async => [Message(id: MessageId('id'))]);
          chatBloc.add(LoadMessagesRequested(null));
        },
        verify: (ChatBloc chatBloc) {
          expect(chatBloc.state.chats.length, 0);
          expect(chatBloc.state.messages.length, 1);
          expect(chatBloc.state.unreadedMessages.length, 0);
        });

    blocTest(
        'messages and unreadedMessages is not be empty on NewMessageReceived',
        build: () => chatBloc,
        act: (ChatBloc chatBloc) {
          when(chatsClientMock.read(any))
              .thenAnswer((realInvocation) async => [Chat(id: ChatId('id'))]);
          when(authBloc.state)
              .thenReturn(AuthenticationAuthenticated(User(id: UserId('id2'))));

          chatBloc.add(LoadChatsRequested());
          chatBloc.add(NewMessageReceived(Message(
              id: MessageId('id'),
              chat: ChatId('id'),
              author: User(id: UserId('id')))));
        },
        verify: (ChatBloc chatBloc) {
          expect(chatBloc.state.chats.length, 1);
          expect(chatBloc.state.messages.length, 1);
          expect(chatBloc.state.unreadedMessages.length, 1);
        });

    blocTest('unreadedMessages should be empty on MessageReaded',
        build: () => chatBloc,
        act: (ChatBloc chatBloc) {
          when(chatsClientMock.read(any))
              .thenAnswer((realInvocation) async => [Chat(id: ChatId('id'))]);
          when(authBloc.state)
              .thenReturn(AuthenticationAuthenticated(User(id: UserId('id2'))));

          chatBloc.add(LoadChatsRequested());
          chatBloc.add(NewMessageReceived(Message(
              id: MessageId('id'),
              chat: ChatId('id'),
              author: User(id: UserId('id')))));
          chatBloc.add(MessageReaded(Message(
              id: MessageId('id'),
              chat: ChatId('id'),
              author: User(id: UserId('id')))));
        },
        verify: (ChatBloc chatBloc) {
          expect(chatBloc.state.chats.length, 1);
          expect(chatBloc.state.messages.length, 1);
          expect(chatBloc.state.unreadedMessages.length, 0);
        });
  });
}
