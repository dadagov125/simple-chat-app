import 'package:bloc_test/bloc_test.dart';
import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/blocs/authentication/authentication_bloc.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../mocks.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('authentication_bloc_test', () {
    AuthenticationBloc authBloc;
    UsersClient usersClient;
    FlutterSecureStorage storage;

    User currentUser = User(
        id: UserId('id'),
        name: 'test',
        email: 'test@test.test',
        phone: '79992220033');

    setUp(() {
      storage = FlutterSecureStorageMock();
      usersClient = UsersClientMock();
      authBloc = AuthenticationBloc(usersClient, storage);

      when(storage.write(key: anyNamed('key'), value: anyNamed('value')))
          .thenAnswer((realInvocation) {});
    });

    blocTest('Should be AuthenticationInitial on initial',
        build: () => authBloc,
        act: (AuthenticationBloc bloc) {},
        expect: [],
        verify: (AuthenticationBloc bloc) {
          expect(bloc.state, AuthenticationUnauthenticated());
        });

    blocTest(
        'Should be AuthenticationUnauthenticated if the token is not stored in the store',
        build: () => authBloc,
        act: (AuthenticationBloc bloc) {
          when(storage.read(key: anyNamed('key')))
              .thenAnswer((realInvocation) async => null);
          when(usersClient.authenticate())
              .thenAnswer((realInvocation) async => null);
          bloc.add(AuthenticateRequested());
        },
        expect: [AuthenticationUnauthenticated()],
        verify: (AuthenticationBloc bloc) {
          expect(bloc.state, AuthenticationUnauthenticated());
        });

    blocTest(
        'Should be AuthenticationAuthenticated if the token is stored in the store',
        build: () => authBloc,
        act: (AuthenticationBloc bloc) {
          when(storage.read(key: anyNamed('key')))
              .thenAnswer((realInvocation) async => 'token');
          when(usersClient.authenticate())
              .thenAnswer((realInvocation) async => currentUser);
          bloc.add(AuthenticateRequested());
        },
        expect: [AuthenticationAuthenticated(currentUser)],
        verify: (AuthenticationBloc bloc) {
          expect(bloc.state, AuthenticationAuthenticated(currentUser));
        });

    blocTest('Should be AuthenticationAuthenticated if login success',
        build: () => authBloc,
        act: (AuthenticationBloc bloc) {
          when(storage.read(key: anyNamed('key')))
              .thenAnswer((realInvocation) async => null);
          when(usersClient.login(any, any))
              .thenAnswer((realInvocation) async => currentUser);
          bloc.add(AuthenticationLoginRequested('', ''));
        },
        expect: [AuthenticationAuthenticated(currentUser)],
        verify: (AuthenticationBloc bloc) {
          expect(bloc.state, AuthenticationAuthenticated(currentUser));
        });

    blocTest('Should be AuthenticationUnauthenticated if login failed',
        build: () => authBloc,
        act: (AuthenticationBloc bloc) {
          when(storage.read(key: anyNamed('key')))
              .thenAnswer((realInvocation) async => null);
          when(usersClient.login(any, any))
              .thenAnswer((realInvocation) async => null);
          bloc.add(AuthenticationLoginRequested('', ''));
        },
        expect: [AuthenticationUnauthenticated()],
        verify: (AuthenticationBloc bloc) {
          expect(bloc.state, AuthenticationUnauthenticated());
        });

    blocTest('Should be AuthenticationUnauthenticated if logout requested',
        build: () => authBloc,
        act: (AuthenticationBloc bloc) {
          when(storage.read(key: anyNamed('key')))
              .thenAnswer((realInvocation) async => null);
          when(usersClient.login(any, any))
              .thenAnswer((realInvocation) async => currentUser);
          bloc.add(AuthenticationLoginRequested('', ''));
          bloc.add(AuthenticationLogoutRequested());
        },
        expect: [
          AuthenticationAuthenticated(currentUser),
          AuthenticationUnauthenticated()
        ],
        verify: (AuthenticationBloc bloc) {
          expect(bloc.state, AuthenticationUnauthenticated());
        });
  });
}
