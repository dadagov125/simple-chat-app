part of 'users_bloc.dart';

@immutable
class UsersState extends Equatable {
  UsersState(this.users, this.checkedUsers);

  final Set<User> users;
  final Set<UserId> checkedUsers;

  UsersState copy({Set<User> users, Set<UserId> checkedUsers}) {
    return UsersState(
        users ?? Set.from(this.users), checkedUsers ?? Set.from(this.checkedUsers));
  }

  @override
  List<Object> get props => [users.toList(), checkedUsers.toList()];
}
