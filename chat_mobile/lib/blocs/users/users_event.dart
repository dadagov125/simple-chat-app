part of 'users_bloc.dart';

@immutable
abstract class UsersEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class LoadUsersRequested extends UsersEvent{

}

class CheckUserRequested extends UsersEvent {
  CheckUserRequested(this.user);

  final User user;
}

class UnCheckUserRequested extends UsersEvent {
  UnCheckUserRequested(this.user);

  final User user;
}

class SaveUserProfileRequested extends UsersEvent {
  final String email;
  final String phone;

  SaveUserProfileRequested(this.email, this.phone);
}
