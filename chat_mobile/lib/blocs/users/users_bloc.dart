import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/blocs/authentication/authentication_bloc.dart';
import 'package:chat_models/chat_models.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'users_event.dart';
part 'users_state.dart';

class UsersBloc extends Bloc<UsersEvent, UsersState> {
  UsersBloc(this.authBloc, this.usersClient) : super(UsersState({}, {}));

  final UsersClient usersClient;

  final AuthenticationBloc authBloc;

  @override
  Stream<UsersState> mapEventToState(
    UsersEvent event,
  ) async* {
    if (event is LoadUsersRequested) {
      yield await _mapLoadUsersRequestedToState(event);
    } else if (event is CheckUserRequested) {
      yield _mapCheckUserRequestedToState(event);
    } else if (event is UnCheckUserRequested) {
      yield _mapUnCheckUserRequestedToState(event);
    } else if (event is SaveUserProfileRequested) {
      _saveUserProfileRequestedhandle(event);
    }
  }

  _saveUserProfileRequestedhandle(SaveUserProfileRequested event) async {
    var currentUser = authBloc.state.currentUser;

    var updatedUser = await usersClient.update(
        User(id: currentUser.id, email: event.email, phone: event.phone));

    authBloc.add(AuthenticationChanged(updatedUser));

    add(LoadUsersRequested());
  }

  _mapLoadUsersRequestedToState(LoadUsersRequested event) async {
    List<User> found = await usersClient.read({});
    return state.copy(users: Set.from(found));
  }

  _mapCheckUserRequestedToState(CheckUserRequested event) {
    var copy = state.copy();
    copy.checkedUsers.add(event.user.id);
    return copy;
  }

  _mapUnCheckUserRequestedToState(UnCheckUserRequested event) {
    var copy = state.copy();
    copy.checkedUsers.removeWhere((u) => u == event.user.id);
    return copy;
  }
}
