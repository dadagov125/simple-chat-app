part of 'chat_bloc.dart';

@immutable
abstract class ChatEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class NewMessageReceived extends ChatEvent {
  NewMessageReceived(this.message);

  final Message message;
}

class MessageReaded extends ChatEvent {
  MessageReaded(this.message);

  final Message message;
}

class LoadChatsRequested extends ChatEvent {}

class LoadMessagesRequested extends ChatEvent {
  LoadMessagesRequested(this.id);

  final ChatId id;
}

class SendMessageRequested extends ChatEvent {
  final Message message;

  SendMessageRequested(this.message);
}

class CreateChatRequested extends ChatEvent {
  CreateChatRequested(this.users);

  final List<User> users;
}
