part of 'chat_bloc.dart';

@immutable
class ChatState extends Equatable {
  ChatState(this.chats, this.messages, this.unreadedMessages);

  final List<Chat> chats;

  final Map<String, Set<Message>> messages;

  final Set<Message> unreadedMessages;

  @override
  List<Object> get props => [chats, unreadedMessages, messages.values];

  ChatState copy(
      {List<Chat> chats,
      Map<String, Set<Message>> messages,
      Set<Message> unreadedMessages}) {
    return ChatState(
        chats ?? List.from(this.chats),
        messages ??
            Map.fromEntries(this
                .messages
                .map((key, value) => MapEntry(key, Set<Message>.from(value)))
                .entries),
        unreadedMessages ?? Set.from(this.unreadedMessages));
  }
}
