import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/blocs/authentication/authentication_bloc.dart';
import 'package:chat_mobile/blocs/notify/notify_bloc.dart';
import 'package:chat_mobile/globals.dart' as globals;
import 'package:chat_models/chat_models.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'chat_event.dart';
part 'chat_state.dart';

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  ChatBloc(
      this.authBloc, this.chatsClient, this.messagesClient, this.notifyBloc)
      : super(ChatState([], {}, {})) {
    connect();
  }

  final AuthenticationBloc authBloc;

  final NotifyBloc notifyBloc;

  final ChatsClient chatsClient;

  final MessagesClient messagesClient;

  WebSocket _webSocket;
  StreamSubscription _wsStreamSubscription;

  Future<void> connect() async {
    _webSocket = await WebSocket.connect(globals.webSocketAddress);
    _wsStreamSubscription = _webSocket?.listen((data) {
      if (data is String) {
        final receivedMessage = Message.fromJson(json.decode(data));
        add(NewMessageReceived(receivedMessage));
      }
    })
      ..onError((err) {
        print(err);
        connect();
      });
  }

  @override
  Stream<ChatState> mapEventToState(
    ChatEvent event,
  ) async* {
    if (event is NewMessageReceived) {
      yield _mapNewMessageReceivedToState(event);
    } else if (event is MessageReaded) {
      yield _mapMessageReadedToState(event);
    } else if (event is LoadChatsRequested) {
      yield await _loadChatsRequestedToState(event);
    } else if (event is LoadMessagesRequested) {
      yield await _loadMessagesRequestedToState(event);
    } else if (event is SendMessageRequested) {
      _sendMessageRequestedHandle(event);
    } else if (event is CreateChatRequested) {
      _createChatRequestedHandle(event);
    }
  }

  void _createChatRequestedHandle(CreateChatRequested event) async {
    await chatsClient.create(Chat(members: event.users));
    add(LoadChatsRequested());
  }

  _sendMessageRequestedHandle(SendMessageRequested event) {
    messagesClient.create(event.message);
  }

  _loadMessagesRequestedToState(LoadMessagesRequested event) async {
    List<Message> messages = await messagesClient.read(event.id);

    var copy = state.copy();

    var chatIdKey = event.id.toString();

    copy.messages[chatIdKey] = Set<Message>.from(messages);

    return copy;
  }

  _loadChatsRequestedToState(LoadChatsRequested event) async {
    List<Chat> found = await chatsClient.read({});
    return state.copy(chats: found);
  }

  _mapNewMessageReceivedToState(NewMessageReceived event) {
    var newMessage = event.message;
    var chatIdKey = newMessage.chat.toString();
    var copy = state.copy();
    var chat = copy.chats.firstWhere((c) => c.id == newMessage.chat);

    if (newMessage.author.id != authBloc.state.currentUser?.id) {
      copy.unreadedMessages.add(newMessage);
      notifyBloc.add(ShowNewMessageNotifyRequested(newMessage, chat));
    }



    if (!copy.messages.containsKey(chatIdKey)) {
      copy.messages[chatIdKey] = Set<Message>.from([newMessage]);
    } else {
      copy.messages[chatIdKey].add(newMessage);
    }

    return copy;
  }

  _mapMessageReadedToState(MessageReaded event) {
    var copy = state.copy();
    copy.unreadedMessages.removeWhere((i) => i.id == event.message.id);
    return copy;
  }

  @override
  Future<void> close() {
    _wsStreamSubscription?.cancel();
    _webSocket?.close();
    return super.close();
  }
}
