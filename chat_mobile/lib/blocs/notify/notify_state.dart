part of 'notify_bloc.dart';

@immutable
abstract class NotifyState extends Equatable {
  @override
  List<Object> get props => [];
}

class NotifyDefault extends NotifyState {}

class NewMessageNotify extends NotifyState {
  NewMessageNotify(this.message, this.chat);

  final Message message;
  final Chat chat;

  @override
  List<Object> get props => [message];
}
