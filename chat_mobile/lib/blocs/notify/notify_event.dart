part of 'notify_bloc.dart';

@immutable
abstract class NotifyEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class ShowNewMessageNotifyRequested extends NotifyEvent {
  ShowNewMessageNotifyRequested(this.message, this.chat);

  final Message message;
  final Chat chat;
}

class DefaultStateRequested extends NotifyEvent {}
