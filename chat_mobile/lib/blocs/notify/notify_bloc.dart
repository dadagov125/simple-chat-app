import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:chat_models/chat_models.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'notify_event.dart';

part 'notify_state.dart';

class NotifyBloc extends Bloc<NotifyEvent, NotifyState> {
  NotifyBloc() : super(NotifyDefault());

  @override
  Stream<NotifyState> mapEventToState(
    NotifyEvent event,
  ) async* {
    if (event is ShowNewMessageNotifyRequested) {
      yield _mapShowNewMessageNotifyRequestedToState(event);
    } else if (event is DefaultStateRequested) {}
  }

  _mapDefaultStateRequestedToState(DefaultStateRequested event) {
    return NotifyDefault();
  }

  _mapShowNewMessageNotifyRequestedToState(
      ShowNewMessageNotifyRequested event) {
    return NewMessageNotify(event.message, event.chat);
  }
}
