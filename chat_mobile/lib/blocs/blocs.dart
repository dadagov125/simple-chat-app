import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/api_client.dart';
import 'package:chat_mobile/blocs/chat/chat_bloc.dart';
import 'package:chat_mobile/blocs/users/users_bloc.dart';
import 'package:chat_mobile/main.dart';
import 'package:chat_mobile/pages/chat_content_page.dart';
import 'package:chat_mobile/pages/home_page.dart';
import 'package:chat_mobile/pages/login_page.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'authentication/authentication_bloc.dart';
import 'notify/notify_bloc.dart';

export 'app_bloc_observer.dart';

List<RepositoryProvider> buildRepositoryProvider(BuildContext context) {
  var storage = FlutterSecureStorage();
  var mobileApiClient = MobileApiClient(storage);
  var usersClient = UsersClient(mobileApiClient);
  var chatsClient = ChatsClient(mobileApiClient);

  var messagesClient = MessagesClient(mobileApiClient);
  return [
    RepositoryProvider<FlutterSecureStorage>(
      create: (_) => storage,
    ),
    RepositoryProvider<MobileApiClient>(
      create: (_) => mobileApiClient,
    ),
    RepositoryProvider<UsersClient>(
      create: (_) => usersClient,
    ),
    RepositoryProvider<ChatsClient>(
      create: (_) => chatsClient,
    ),
    RepositoryProvider<MessagesClient>(
      create: (_) => messagesClient,
    )
  ];
}

List<BlocProvider> buildAppBlocProviders(BuildContext context) {
  var usersClient = RepositoryProvider.of<UsersClient>(context);
  var chatsClient = RepositoryProvider.of<ChatsClient>(context);
  var messagesClient = RepositoryProvider.of<MessagesClient>(context);
  var storage = RepositoryProvider.of<FlutterSecureStorage>(context);

  AuthenticationBloc authenticationBloc =
      AuthenticationBloc(usersClient, storage);

  var notifyBloc = NotifyBloc();
  return [
    BlocProvider<AuthenticationBloc>(
        create: (context) => authenticationBloc, lazy: false),
    BlocProvider<ChatBloc>(
        create: (context) => ChatBloc(
            authenticationBloc, chatsClient, messagesClient, notifyBloc),
        lazy: false),
    BlocProvider<UsersBloc>(
        create: (context) => UsersBloc(authenticationBloc, usersClient),
        lazy: false),
    BlocProvider<NotifyBloc>(
      create: (context) => notifyBloc,
      lazy: false,
    )
  ];
}

List<BlocListener> buildAppBlocListeners(BuildContext context) {
  return [
    BlocListener<AuthenticationBloc, AuthenticationState>(
        listener: (context, state) {
          if (state is AuthenticationAuthenticated) {
            SimpleChatApp.navigator
                .pushAndRemoveUntil(HomePage.route(), (route) => false);
          } else if (state is AuthenticationUnauthenticated) {
            SimpleChatApp.navigator
                .pushAndRemoveUntil(LoginPage.route(), (route) => false);
          }
        }),
    BlocListener<NotifyBloc, NotifyState>(
      listenWhen: (prev, current) => current is NewMessageNotify,
      listener: (context, state) {
        if (state is NewMessageNotify) {
          Message message = state.message;
          Chat chat = state.chat;
          var text = message.text.isNotEmpty && message.text.length > 30
              ? message.text.substring(0, 30) + '....'
              : message.text;
          Flushbar(
            title: message.author.name,
            message: text,
            flushbarPosition: FlushbarPosition.TOP,
            flushbarStyle: FlushbarStyle.FLOATING,
            icon: Icon(
              Icons.message,
              color: Colors.white,
            ),
            leftBarIndicatorColor: Colors.blue,
            dismissDirection: FlushbarDismissDirection.HORIZONTAL,
            isDismissible: true,
            animationDuration: Duration(milliseconds: 200),
            duration: Duration(seconds: 10),
            onTap: (flushbar) {
              SimpleChatApp.navigator
                  .pushAndRemoveUntil(ChatContentPage.route(chat),
                      (route) => route.isFirst);
            },
          ).show(SimpleChatApp.navigator.context);

          BlocProvider.of<NotifyBloc>(context).add(DefaultStateRequested());
        }
      },
    )
  ];
}
