part of 'authentication_bloc.dart';

@immutable
abstract class AuthenticationState extends Equatable {
  @override
  List<Object> get props => [this.runtimeType.toString()];
}

// class AuthenticationInitial extends AuthenticationState {}

class AuthenticationUnauthenticated extends AuthenticationState {}

class AuthenticationAuthenticated extends AuthenticationState {
  AuthenticationAuthenticated(this.user) : assert(user != null);

  final User user;

  @override
  List<Object> get props => [this.user];
}

extension AuthenticationStateExt on AuthenticationState {
  User get currentUser {
    if (this is AuthenticationAuthenticated) {
      return (this as AuthenticationAuthenticated).user;
    }
  }
}
