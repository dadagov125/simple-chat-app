import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/globals.dart' as globals;
import 'package:chat_models/chat_models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:meta/meta.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc(this.usersClient, this.storage)
      : super(AuthenticationUnauthenticated()) {
    _userSubscription = _userStreamController.stream.listen((event) {
      add(AuthenticationChanged(event));
    });
  }

  final FlutterSecureStorage storage;
  final UsersClient usersClient;

  StreamSubscription<User> _userSubscription;
  StreamController<User> _userStreamController = StreamController.broadcast();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AuthenticationChanged) {
      yield _mapAuthenticationChangedToState(event);
    } else if (event is AuthenticationLoginRequested) {
      _authenticationLoginRequestedHandle(event);
    } else if (event is AuthenticationLogoutRequested) {
      _authenticationLogoutRequestedHandle();
    } else if (event is AuthenticateRequested) {
      _authenticateRequestedHandle(event);
    }
  }

  _authenticationLoginRequestedHandle(
      AuthenticationLoginRequested event) async {
    var user = await usersClient.login(event.username, event.password);
    _userStreamController.add(user);
  }

  _authenticationLogoutRequestedHandle() async {
    await storage.delete(key: globals.authTokenKey);
    _userStreamController.add(null);
  }

  AuthenticationState _mapAuthenticationChangedToState(
      AuthenticationChanged event) {
    if (event.user != null) {
      return AuthenticationAuthenticated(event.user);
    } else {
      return AuthenticationUnauthenticated();
    }
  }

  @override
  Future<void> close() {
    _userSubscription?.cancel();
    _userStreamController.close();
    return super.close();
  }

  void _authenticateRequestedHandle(AuthenticateRequested event) async {
    try {
      var user = await usersClient.authenticate();
      add(AuthenticationChanged(user));
    } on Exception {
      add(AuthenticationChanged(null));
    }
  }
}
