part of 'authentication_bloc.dart';

@immutable
abstract class AuthenticationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class AuthenticationChanged extends AuthenticationEvent {
  AuthenticationChanged(this.user);

  final User user;

  @override
  List<Object> get props => [user];
}

class AuthenticationLogoutRequested extends AuthenticationEvent {}

class AuthenticationLoginRequested extends AuthenticationEvent {
  AuthenticationLoginRequested(this.username, this.password);

  final String username;
  final String password;

  @override
  List<Object> get props => [username, password];
}

class AuthenticateRequested extends AuthenticationEvent {}
