import 'package:chat_mobile/blocs/authentication/authentication_bloc.dart';
import 'package:chat_mobile/blocs/users/users_bloc.dart';
import 'package:chat_mobile/pages/profile_page/cubit/profile_cubit.dart';
import 'package:chat_mobile/widgets/logout_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

class ProfilePage extends StatelessWidget {
  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => ProfilePage());
  }

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UsersBloc, UsersState>(
      builder: (context, state) {
        return BlocProvider<ProfileCubit>(
            create: (_) => ProfileCubit(BlocProvider.of<UsersBloc>(context),
                BlocProvider.of<AuthenticationBloc>(context)),
            child: BlocBuilder<ProfileCubit, ProfileState>(
              builder: (context, state) {
                return Scaffold(
                    appBar: AppBar(
                      title: Text('Profile'),
                      actions: <Widget>[LogoutButton()],
                    ),
                    floatingActionButton: state.status.isValid
                        ? FloatingActionButton(
                            onPressed: () {
                              BlocProvider.of<ProfileCubit>(context).save();
                            },
                            child: Icon(Icons.save))
                        : null,
                    body: Container(
                      margin: EdgeInsets.all(10),
                      child: Center(
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              TextFormField(
                                initialValue: state.email.value,
                                decoration: InputDecoration(
                                    hintText: 'enter your email',
                                    labelText: 'Email',
                                    errorText: state.email.invalid
                                        ? "invalid email"
                                        : null),
                                keyboardType: TextInputType.emailAddress,
                                onChanged: (value) =>
                                    BlocProvider.of<ProfileCubit>(context)
                                        .emailChanged(value),
                              ),
                              TextFormField(
                                initialValue: state.phone.value,
                                keyboardType: TextInputType.phone,
                                decoration: InputDecoration(
                                    hintText: 'enter your phone number',
                                    labelText: 'Phone',
                                    errorText: state.phone.invalid
                                        ? 'invalid phone number'
                                        : null),
                                onChanged: (value) =>
                                    BlocProvider.of<ProfileCubit>(context)
                                        .phoneChanged(value),
                              )
                            ],
                          ),
                        ),
                      ),
                    ));
              },
            ));
      },
    );
  }
}
