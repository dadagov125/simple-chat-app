import 'package:formz/formz.dart';

enum PhoneValidationError { invalid }

class PhoneModel extends FormzInput<String, PhoneValidationError> {
  const PhoneModel.pure([String value]) : super.pure(value ?? '');

  const PhoneModel.dirty([String value]) : super.dirty(value);

  static final RegExp _phoneRegExp = RegExp(
    r"^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$",
  );

  @override
  PhoneValidationError validator(String value) {
    var res = value.isNotEmpty && _phoneRegExp.hasMatch(value)
        ? null
        : PhoneValidationError.invalid;

    return res;
  }
}
