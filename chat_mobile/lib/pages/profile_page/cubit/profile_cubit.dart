import 'package:bloc/bloc.dart';
import 'package:chat_mobile/blocs/authentication/authentication_bloc.dart';
import 'package:chat_mobile/blocs/users/users_bloc.dart';
import 'package:chat_mobile/pages/profile_page/models/email_model.dart';
import 'package:chat_mobile/pages/profile_page/models/phone_model.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:meta/meta.dart';

part 'profile_state.dart';

class ProfileCubit extends Cubit<ProfileState> {
  ProfileCubit(this.usersBloc, this.authBloc)
      : super(ProfileState.from(authBloc.state.currentUser?.email,
            authBloc.state.currentUser?.phone));
  final AuthenticationBloc authBloc;
  final UsersBloc usersBloc;

  emailChanged(String value) {
    final email = EmailModel.dirty(value);
    var validate = Formz.validate([email, state.phone]);
    emit(state.copy(email: email, status: validate));
  }

  phoneChanged(String value) {
    final phone = PhoneModel.dirty(value);
    var validate = Formz.validate([state.email, phone]);
    emit(state.copy(phone: phone, status: validate));
  }

  void save() {
    if (!state.status.isValidated) return;

    state.copy(status: FormzStatus.submissionInProgress);

    try {
      usersBloc
          .add(SaveUserProfileRequested(state.email.value, state.phone.value));

      state.copy(status: FormzStatus.submissionSuccess);
    } on Exception {
      state.copy(status: FormzStatus.submissionFailure);
    }
  }
}
