part of 'profile_cubit.dart';

@immutable
class ProfileState extends Equatable {
  ProfileState(
      {this.email = const EmailModel.pure(),
      this.phone = const PhoneModel.pure(),
      this.status = FormzStatus.pure});

  final EmailModel email;
  final PhoneModel phone;
  final FormzStatus status;

  ProfileState copy({EmailModel email, PhoneModel phone, FormzStatus status}) {
    return ProfileState(
      email: email ?? this.email,
      phone: phone ?? this.phone,
      status: status ?? this.status,
    );
  }

  factory ProfileState.from(String email, String phone) {
    return ProfileState(
        email: EmailModel.pure(email),
        phone: PhoneModel.pure(phone),
        status: FormzStatus.pure);
  }

  @override
  List<Object> get props => [email, phone, status];
}
