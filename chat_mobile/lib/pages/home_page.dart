import 'package:chat_mobile/widgets/chat_list.dart';
import 'package:chat_mobile/widgets/logout_button.dart';
import 'package:chat_mobile/widgets/profile_button.dart';
import 'package:chat_mobile/widgets/user_list.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => HomePage());
  }

  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          actions: <Widget>[LogoutButton(), ProfileButton()],
          automaticallyImplyLeading: false,
        ),
        body: TabBarView(
          children: <Widget>[ChatList(), UserList()],
        ),
        bottomNavigationBar: TabBar(
          tabs: <Widget>[
            Tab(
              icon: new Icon(Icons.chat),
            ),
            Tab(
              icon: new Icon(Icons.supervised_user_circle),
            ),
          ],
          labelColor: Colors.lightBlueAccent,
          unselectedLabelColor: Colors.grey,
          indicatorSize: TabBarIndicatorSize.label,
        ),
      ),
    );
  }
}
