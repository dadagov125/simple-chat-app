import 'dart:collection';

import 'package:chat_mobile/blocs/authentication/authentication_bloc.dart';
import 'package:chat_mobile/blocs/chat/chat_bloc.dart';
import 'package:chat_mobile/widgets/logout_button.dart';
import 'package:chat_mobile/widgets/profile_button.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:visibility_detector/visibility_detector.dart';

class ChatContentPage extends StatefulWidget {
  ChatContentPage({Key key, @required this.chat}) : super(key: key);

  static route(Chat chat) => MaterialPageRoute(
        builder: (context) {
          return ChatContentPage(
            chat: chat,
          );
        },
      );
  final Chat chat;
  final formatter = DateFormat('HH:mm');

  @override
  _ChatContentPageState createState() => _ChatContentPageState();
}

class _ChatContentPageState extends State<ChatContentPage> {
  String _title = 'COntent page';

  final _sendMessageTextController = TextEditingController();
  Set<ChatId> _unreadChats = HashSet<ChatId>();

  @override
  void initState() {
    super.initState();

    BlocProvider.of<ChatBloc>(context)
        .add(LoadMessagesRequested(widget.chat.id));
  }

  @override
  void dispose() {
    _sendMessageTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var chatIdKey = widget.chat.id.toString();

    return BlocBuilder<AuthenticationBloc, AuthenticationState>(
      builder: (context, authState) {
        return Scaffold(
          appBar: AppBar(
            actions: <Widget>[LogoutButton(), ProfileButton()],
          ),
          body: BlocBuilder<ChatBloc, ChatState>(builder: (context, state) {
            List<Message> messages = state.messages.containsKey(chatIdKey)
                ? state.messages[chatIdKey].toList()
                : [];

            return Container(
              padding:
                  const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: ListView.builder(
                      itemCount: messages.length,
                      itemBuilder: (BuildContext context, int index) {
                        var message = messages[index];
                        var isUnreaded = state.unreadedMessages.any((u) =>
                            message.id == u.id && u.chat == widget.chat.id);

                        var isMyMessage =
                            message.author.id == authState.currentUser?.id;

                        return _buildListTile(message, isUnreaded, isMyMessage);
                      },
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          controller: _sendMessageTextController,
                          decoration: InputDecoration(hintText: 'Your message'),
                        ),
                      ),
                      IconButton(
                        icon: const Icon(Icons.send),
                        onPressed: () {
                          if (_sendMessageTextController.text.isNotEmpty) {
                            send(_sendMessageTextController.text,
                                authState.currentUser);
                          }
                        },
                      )
                    ],
                  ),
                ],
              ),
            );
          }),
        );
      },
    );
  }

  _onVisibilityChanged(VisibilityInfo info) {
    var message = (info.key as ValueKey<Message>).value;
    BlocProvider.of<ChatBloc>(context).add(MessageReaded(message));
  }

  Widget _buildListTile(Message message, bool isUnreaded, bool isMyMessage) {
    var messageTime = widget.formatter.format(message.createdAt);

    return isUnreaded && !isMyMessage
        ? VisibilityDetector(
            key: ValueKey(message),
            onVisibilityChanged: _onVisibilityChanged,
            child: _Bubble(
              message: message.text,
              isMe: isMyMessage,
              time: messageTime,
              isUnreaded: isUnreaded,
            ),
          )
        : _Bubble(
            message: message.text,
            isMe: isMyMessage,
            time: messageTime,
            isUnreaded: isUnreaded,
          );
  }

  send(String message, User currentUser) async {
    final newMessage = Message(
        chat: widget.chat.id,
        author: currentUser,
        text: message,
        createdAt: DateTime.now());
    BlocProvider.of<ChatBloc>(context).add(SendMessageRequested(newMessage));
    _sendMessageTextController.clear();
  }
}

class _Bubble extends StatelessWidget {
  _Bubble({this.message, this.time, this.isMe, this.isUnreaded});

  final String message, time;
  final isMe;
  final isUnreaded;

  @override
  Widget build(BuildContext context) {
    final bg = isMe
        ? Colors.white
        : isUnreaded ? Colors.blueAccent.shade100 : Colors.greenAccent.shade100;
    final align = isMe ? CrossAxisAlignment.start : CrossAxisAlignment.end;
    final radius = isMe
        ? BorderRadius.only(
            topRight: Radius.circular(5.0),
            bottomLeft: Radius.circular(10.0),
            bottomRight: Radius.circular(5.0),
          )
        : BorderRadius.only(
            topLeft: Radius.circular(5.0),
            bottomLeft: Radius.circular(5.0),
            bottomRight: Radius.circular(10.0),
          );
    return Column(
      crossAxisAlignment: align,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.all(3.0),
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  blurRadius: .5,
                  spreadRadius: 1.0,
                  color: Colors.black.withOpacity(.12))
            ],
            color: bg,
            borderRadius: radius,
          ),
          child: Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 48.0),
                child: Text(message),
              ),
              Positioned(
                bottom: 0.0,
                right: 0.0,
                child: Row(
                  children: <Widget>[
                    Text(time,
                        style: TextStyle(
                          color: Colors.black38,
                          fontSize: 10.0,
                        )),
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
