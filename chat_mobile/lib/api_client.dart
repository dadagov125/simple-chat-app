import 'package:chat_api_client/chat_api_client.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'globals.dart' as globals;

class MobileApiClient extends ApiClient {
  MobileApiClient(FlutterSecureStorage storage)
      : super(Uri.parse(globals.chatApiAddress),
            onBeforeRequest: (ApiRequest request) async {
          final authToken = await storage.read(key: globals.authTokenKey);
          if (authToken != null)
            return request.change(
                headers: {}
                  ..addAll(request.headers)
                  ..addAll({globals.authTokenKey: authToken}));
          return request;
        }, onAfterResponse: (ApiResponse response) async {
          if (response.headers.containsKey(globals.authTokenKey)) {
            var authToken = response.headers[globals.authTokenKey];
            await storage.write(key: globals.authTokenKey, value: authToken);
          }

          return response;
        });
}
