import 'dart:async';

import 'package:chat_mobile/blocs/chat/chat_bloc.dart';
import 'package:chat_mobile/pages/chat_content_page.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChatList extends StatefulWidget {
  ChatList({Key key}) : super(key: key);
  final String title = 'title';

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => ChatList());
  }

  @override
  _ChatListState createState() => _ChatListState();
}

class _ChatListState extends State<ChatList> {
  Completer<void> _refreshCompleter;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<ChatBloc>(context).add(LoadChatsRequested());
    _refreshCompleter = Completer<void>();
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool _hasUnreadedMessages(Chat chatItem, ChatState state) {
    if (state.unreadedMessages.isNotEmpty) {
      state.unreadedMessages.any((m) => m.chat == chatItem.id);
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatBloc, ChatState>(
      builder: (context, state) {
        Iterable<Widget> listTiles = state.chats.map<Widget>((Chat chatItem) =>
            _buildListTile(chatItem, _hasUnreadedMessages(chatItem, state)));
        listTiles = ListTile.divideTiles(context: context, tiles: listTiles);

        _refreshCompleter?.complete();
        _refreshCompleter = Completer<void>();

        return Scaffold(
            body: Column(
          children: <Widget>[
            Expanded(
              child: RefreshIndicator(
                color: Colors.black,
                backgroundColor: Colors.lightBlueAccent,
                onRefresh: () async {
                  print('refreshing');
                  BlocProvider.of<ChatBloc>(context).add(LoadChatsRequested());

                  return _refreshCompleter.future;
                },
                child: ListView(
                  key: PageStorageKey('chat_list_view'),
                  children: listTiles.toList(),
                ),
              ),
            ),
          ],
        ));
      },
    );
  }

  Widget _buildListTile(Chat chat, bool hasUnreadedMessage) {
    return Container(
      child: ListTile(
        leading: hasUnreadedMessage ? const Icon(Icons.message) : null,
        title: Text(chat.members.map((user) => user.name).join(", ")),
        onTap: () {
          Navigator.of(context).push(ChatContentPage.route(chat));
        },
      ),
    );
  }
}
