import 'package:chat_mobile/main.dart';
import 'package:chat_mobile/pages/profile_page/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ProfileButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.account_box),
        tooltip: 'Profile',
        onPressed: () {
          SimpleChatApp.navigator.push(ProfilePage.route());
        });
  }
}
