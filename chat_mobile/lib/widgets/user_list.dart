import 'dart:async';

import 'package:chat_mobile/blocs/authentication/authentication_bloc.dart';
import 'package:chat_mobile/blocs/chat/chat_bloc.dart';
import 'package:chat_mobile/blocs/users/users_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  Completer<void> _refreshCompleter;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<UsersBloc>(context).add(LoadUsersRequested());
    _refreshCompleter = Completer<void>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthenticationBloc, AuthenticationState>(
      builder: (context, authState) {
        return BlocBuilder<UsersBloc, UsersState>(builder: (context, state) {
          _refreshCompleter?.complete();
          _refreshCompleter = Completer<void>();
          return Scaffold(
            floatingActionButton: state.checkedUsers.isEmpty
                ? null
                : FloatingActionButton(
                    onPressed: () {
                      var newChatUsers = state.users
                          .where((u) => state.checkedUsers.contains(u.id))
                          .toList()
                            ..add(authState.currentUser);
                      BlocProvider.of<ChatBloc>(context)
                          .add(CreateChatRequested(newChatUsers));
                    },
                    child: Icon(Icons.add)),
            body: ListTileTheme(
              selectedColor: Colors.blue,
              child: RefreshIndicator(
                color: Colors.black,
                backgroundColor: Colors.lightBlueAccent,
                onRefresh: () async {
                  print('refreshing');
                  BlocProvider.of<UsersBloc>(context).add(LoadUsersRequested());

                  return _refreshCompleter.future;
                },
                child: ListView(
                  key: PageStorageKey('user_list_view'),
                  children: _buildUserListTile(state),
                ),
              ),
            ),
          );
        });
      },
    );
  }

  List<Widget> _buildUserListTile(UsersState state) {
    return state.users.map((u) {
      var isChecked = state.checkedUsers.contains(u.id);
      return ListTile(
        leading: Container(
            height: 50,
            width: 50,
            decoration: new BoxDecoration(
              color: Colors.lightBlueAccent.shade100,
              // border: Border.all(color: Colors.blueAccent, width: 1.0),
              borderRadius: new BorderRadius.all(Radius.circular(25)),
            ),
            child: Center(
              child: Text(
                u.name.substring(0, 1).toUpperCase(),
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w700),
              ),
            )),
        title: Text(u.name ?? ''),
        subtitle: Text(u.email ?? ''),
        selected: isChecked,
        onTap: () {
          BlocProvider.of<UsersBloc>(context)
              .add(isChecked ? UnCheckUserRequested(u) : CheckUserRequested(u));
        },
      );
    }).toList();
  }
}
