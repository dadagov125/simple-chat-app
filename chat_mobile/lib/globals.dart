library chat_mobile.globals;

const String host = '10.0.2.2';
const String webSocketAddress = 'ws://$host:3333/ws';
const String chatApiAddress = 'http://$host:3333';

// String authToken;
// User currentUser;

const String authTokenKey = 'authorization';
