import 'package:bloc/bloc.dart';
import 'package:chat_mobile/blocs/blocs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'widgets/splash_screen.dart';

void main() {
  Bloc.observer = AppBlocObserver();

  runApp(SimpleChatApp());
}

class SimpleChatApp extends StatefulWidget {
  static final navigatorKey = GlobalKey<NavigatorState>();

  static NavigatorState get navigator => navigatorKey.currentState;

  @override
  _SimpleChatAppState createState() => _SimpleChatAppState();
}

class _SimpleChatAppState extends State<SimpleChatApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: buildRepositoryProvider(context),
      child: Builder(
        builder: (context) {
          return MultiBlocProvider(
            providers: buildAppBlocProviders(context),
            child: MaterialApp(
                navigatorKey: SimpleChatApp.navigatorKey,
                title: 'Simple Chat',
                theme: ThemeData(
                  primarySwatch: Colors.blue,
                ),
                builder: (context, child) {
                  return MultiBlocListener(
                    listeners: buildAppBlocListeners(context),
                    child: child,
                  );
                },
                onGenerateRoute: (_) => SplashScreen.route()),
          );
        },
      ),
    );
  }
}
